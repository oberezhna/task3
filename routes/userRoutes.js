const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();
// USER:
//         GET /api/users
//         GET /api/users/:id
//         POST /api/users
//         PUT /api/users/:id
//         DELETE /api/users/:id
// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {
    try {
        const users = UserService.getUsers()
        res.data = users;
    } catch (err) {
        res.status(404).send(`Something failed`);   
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    const id = req.params.id
    try {
        const user = UserService.search(id)
        res.data = user;
    } catch (err) {
        res.status(404).send(`Something failed`);   
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    //validation
    try {
        const newUser= req.params.userData
        const user = UserService.createUser(newUser);
        res.data = user;
    } catch (err) {
        res.status(404).send(`Something failed`);   
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    const id = req.params.id;
    const dataToUpdate = req.params.userData;
    try {
        const updatedUser = UserService.updateUser(id, dataToUpdate)
        res.data = updatedUser;
    } catch (err) {
        res.status(404).send(`Something failed`);   
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    const id = req.params.id
    try {
        const result = UserService.deleteUser(id)
        res.data = result;
    } catch (err) {
        res.status(404).send(`Something failed`);   
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;