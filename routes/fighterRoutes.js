const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

        // GET /api/fighters
        // GET /api/fighters/:id
        // POST /api/fighters
        // PUT /api/fighters/:id
        // DELETE /api/fighters/:id
// TODO: Implement route controllers for fighter
router.get('/', (req, res, next) => {
    try {
        const fighters = FighterService.getfighters()
        res.data = fighters;
    } catch (err) {
        res.status(404).send(`Users not found`);   
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    const id = req.params.id
    try {
        const fighter = FighterService.search(id)
        res.data = fighter;
    } catch (err) {
        res.status(404).send(`User not found`);   
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    try {
        const newFighter= req.params.fighterData
        const fighter = FighterService.createfighter(newFighter);
        res.data = fighter;
    } catch (err) {
        res.status(404).send(`Something failed`);   
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    const id = req.params.id;
    const dataToUpdate = req.params.fighterData;
    try {
        const updatedFighter = FighterService.updatefighter(id, dataToUpdate)
        res.data = updatedFighter;
    } catch (err) {
        res.status(404).send(`Something failed`);   
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    const id = req.params.id
    try {
        const result = FighterService.deleteFighter(id)
        res.data = result;
    } catch (err) {
        res.status(404).send(`Something failed`);   
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);
module.exports = router;