const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
         const { body: userData } = req;
        // const result = req.body
        const user = AuthService.login(userData);
        res.data = user;
    } catch (err) {
        res.err = err;
        res.status(404).send(`Something failed`);   
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;