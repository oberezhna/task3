const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getUsers() {
        const users = UserRepository.getAll();
        if(!users){
            throw Error('No users');
        }
        return users;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    createUser(data){
        const newUser = UserRepository.create(data);
        if(!newUser){
            throw Error('User wasn\'t created');
        }
        return newUser;
    }

    updateUser(id, dataToUpdate){
        const updatedUser = UserRepository.update(id, dataToUpdate);
        if(!updatedUser){
            throw Error('User wasn\'t updated');
        }
        return updatedUser;
    }

    deleteUser(id){
        const result = UserRepository.delete(id);
        if(!result){
            throw Error('User wasn\'t deteted');
        }
        else return result;
    }
}

module.exports = new UserService();