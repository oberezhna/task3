const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getFighters() {
        const fighters = FighterRepository.getAll();
        if(!fighters){
            throw Error('No fighters');
        }
        return fighters;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    createfighter(data){
        const newFighter = FighterRepository.create(data);
        if(!newFighter){
            throw Error('Fighter wasn\'t created');
        }
        return newfighter;
    }

    updatefighter(id, dataToUpdate){
        const updatedFighter = FighterRepository.update(id, dataToUpdate);
        if(!updatedFighter){
            throw Error('Fighter wasn\'t updated');
        }
        return updatedFighter;
    }

    deletefighter(id){
        const result = FighterRepository.delete(id);
        if(!result){
            throw Error('Fighter wasn\'t deteted');
        }
        else return result;
    }
}

module.exports = new FighterService();